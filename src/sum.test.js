const sum = require("./sum.js");

test('adds 1 + 2 to equal 3', () => {
    expect(sum(1, 2)).toBe(3);
  });

  test('adds 4 in string + 5 in string to equal 9', () => {
    expect(sum('4', '5')).toBe(9);
  });